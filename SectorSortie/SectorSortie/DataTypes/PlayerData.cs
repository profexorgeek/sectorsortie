﻿using SectorSortie.Entities.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.DataTypes
{
    public class PlayerData
    {
        public string Name { get; set; }
        public ShipData Ship { get; set; }
        public int WavesPlayed { get; set; }
        public int CurrentCash { get; set; }
        public int HighestCompletedWave { get; set; }
        public int PlayerIndex { get; set; }
        public bool InUse { get; set; }
    }
}
