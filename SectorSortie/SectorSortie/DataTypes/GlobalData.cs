﻿using SectorSortie.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SectorSortie.DataTypes
{
    public static class GlobalData
    {
        public const string ApplicationSettingsDirectory = "SectorSortie";
        public static ILogger Logger = new BasicLogger();
        private static List<PlayerData> mSavedPlayers;

        /// <summary>
        /// The path to the player data file
        /// </summary>
        public static String PlayerDataFilePath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ApplicationSettingsDirectory, "players.dat");
            }
        }

        /// <summary>
        /// The list of SavedPlayers that is stored in the users' AppData directory
        /// </summary>
        public static List<PlayerData> SavedPlayers
        {
            get
            {
                if(mSavedPlayers == null)
                {
                    mSavedPlayers = LoadPlayers();
                }
                return mSavedPlayers;
            }
            set
            {
                mSavedPlayers = value;
                SavePlayers();
            }
        }

        /// <summary>
        /// Makes sure application directory exists and then loads any saved players
        /// </summary>
        public static void Initialize() {
            CheckCreateApplicationDirectory();
            mSavedPlayers = LoadPlayers();
        }

        /// <summary>
        /// Attempts to load saved player data from the application's data directory
        /// </summary>
        /// <returns>List of saved players</returns>
        public static List<PlayerData> LoadPlayers() {
            XmlSerializer serializer = new XmlSerializer(typeof(List<PlayerData>));
            List<PlayerData> players = new List<PlayerData>();
            try {
                using(FileStream file = File.Open(PlayerDataFilePath, FileMode.OpenOrCreate)) {

                    players = serializer.Deserialize(file) as List<PlayerData>;
                }
            }
            catch(Exception e) {
                Logger.LogWarning("Failed to load player data - " + e.Message);
            }
            return players;
        }

        /// <summary>
        /// Saves the player list to disk
        /// </summary>
        public static void SavePlayers() {
            try {
                using(FileStream file = File.Open(PlayerDataFilePath, FileMode.OpenOrCreate)) {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<PlayerData>));

                    serializer.Serialize(file, mSavedPlayers);
                }
            }
            catch(Exception e) {
                Logger.LogWarning("Failed to save player data - " + e.Message);
            }
        }

        /// <summary>
        /// Attempts to add a new player and then saves the list back to disk.
        /// Throws an exception if the player name already exists.
        /// </summary>
        /// <param name="name">The name of the player to add</param>
        /// <returns>A PlayerData object representing the new player</returns>
        public static PlayerData AddNewPlayer(string name) {

            // trim leading and trailing characters
            name = name.Trim(new char[2] { ' ', '_' });
            if(string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Cannot save empty name!");
            }

            foreach(PlayerData player in SavedPlayers) {
                if(name == player.Name) {
                    throw new Exception("Player name already exists");
                }
            }

            ShipData newShip = new ShipData() {
                FrontWeapon = GlobalContent.ShotTypes[ShotTypes.RepeaterLite],
                ShipType = GlobalContent.ShipTypes[ShipTypes.Junebug]
            };

            PlayerData newPlayer = new PlayerData() {
                Name = name,
                WavesPlayed = 0,
                Ship = newShip,
                CurrentCash = 0,
                InUse = false
            };

            SavedPlayers.Add(newPlayer);
            SavePlayers();

            return newPlayer;
        }

        /// <summary>
        /// Permenantly removes any players with the specified name
        /// </summary>
        /// <param name="name">The name of the player to be removed</param>
        public static void DeletePlayer(PlayerData player)
        {
            for(int i = mSavedPlayers.Count - 1; i > -1; i--)
            {
                if(mSavedPlayers[i] == player)
                {
                    mSavedPlayers.RemoveAt(i);
                }
            }
            SavePlayers();
        }

        /// <summary>
        /// Attempts to create a data storing directory for the application if it doesn't exist.
        /// </summary>
        public static void CheckCreateApplicationDirectory()
        {
            string dirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ApplicationSettingsDirectory);
            if(!Directory.Exists(dirPath)) {
                Directory.CreateDirectory(dirPath);
            }
        }
    }
}
