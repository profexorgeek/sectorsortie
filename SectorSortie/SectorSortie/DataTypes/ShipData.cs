﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.DataTypes
{
    public class ShipData
    {
        public ShipTypes ShipType { get; set; }
        public ShotTypes FrontWeapon { get; set; }
        public ShotTypes LeftWeapon { get; set; }
        public ShotTypes RightWeapon { get; set; }
        public ShotTypes RearWeapon { get; set; }
    }
}
