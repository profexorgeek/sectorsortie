using System;
using System.Collections.Generic;

using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Utilities;

using Microsoft.Xna.Framework;
#if !FRB_MDX
using System.Linq;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
#endif
using FlatRedBall.Screens;
using SectorSortie.DataTypes;
using FlatRedBall.Input;

namespace SectorSortie
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
			
#if WINDOWS_PHONE || ANDROID || IOS

			// Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);
            graphics.IsFullScreen = true;
#else
            graphics.PreferredBackBufferHeight = 600;
#endif
        }

        protected override void Initialize()
        {
            Renderer.UseRenderTargets = false;
            FlatRedBallServices.InitializeFlatRedBall(this, graphics);
			GlobalContent.Initialize();
            GlobalData.Initialize();
			CameraSetup.SetupCamera(SpriteManager.Camera, graphics);
			FlatRedBall.Screens.ScreenManager.Start(typeof(SectorSortie.Screens.MainMenu));

#if DEBUG
            DefineKeyboardMap();
#endif

            base.Initialize();
        }


        protected override void Update(GameTime gameTime)
        {
            FlatRedBallServices.Update(gameTime);

            FlatRedBall.Screens.ScreenManager.Activity();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            FlatRedBallServices.Draw();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Maps gamepad buttons to the keyboard to make testing easier
        /// </summary>
        private void DefineKeyboardMap()
        {
            Xbox360GamePad g = InputManager.Xbox360GamePads[0];
            if(!g.IsConnected)
            {
                g.ButtonMap = new KeyboardButtonMap();
                
                g.ButtonMap.LeftAnalogUp = Keys.W;
                g.ButtonMap.LeftAnalogDown = Keys.S;
                g.ButtonMap.LeftAnalogLeft = Keys.A;
                g.ButtonMap.LeftAnalogRight = Keys.D;

                g.ButtonMap.RightAnalogUp = Keys.Up;
                g.ButtonMap.RightAnalogDown = Keys.Down;
                g.ButtonMap.RightAnalogLeft = Keys.Left;
                g.ButtonMap.RightAnalogRight = Keys.Right;

                g.ButtonMap.LeftShoulder = Keys.Q;
                g.ButtonMap.RightShoulder = Keys.E;

                g.ButtonMap.LeftTrigger = Keys.RightAlt;
                g.ButtonMap.RightTrigger = Keys.RightControl;
                g.ButtonMap.A = Keys.Space;
                g.ButtonMap.B = Keys.OemBackslash;

                g.FakeIsConnected = true;
            }
        }
    }
}
