using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
namespace SectorSortie.Entities.Gui
{
	public partial class NameSelector
	{
		void OnAfterSelectedIndexSet (object sender, EventArgs e)
        {
            if(SelectedIndex >= Characters.Count) {
                mSelectedIndex = 0;
            }
            if(SelectedIndex < 0) {
                mSelectedIndex = Characters.Count - 1;
            }

            UpdateSelectedState();
        }
	}
}
