using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using SectorSortie.DataTypes;


#endif

namespace SectorSortie.Entities.Gui
{
    public partial class PlayerSelector
    {
        private void CustomInitialize()
        {
            CurrentVisualState = VisualState.Normal;
        }

        private void CustomActivity()
        {

        }

        private void CustomDestroy()
        {


        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        /// <summary>
        /// Override to set appearance based on Visual state
        /// </summary>
        public override MenuItemBase.VisualState CurrentVisualState
        {
            get
            {
                return mCurrentVisualState;
            }
            set
            {
                mCurrentVisualState = value;
                switch(mCurrentVisualState)
                {
                    case VisualState.Normal:
                    case VisualState.Pressed:
                        LeftArrow.Visible = false;
                        RightArrow.Visible = false;
                        NameTextCurrentState = TextMedium.VariableState.Normal;
                        break;
                    case VisualState.Selected:
                        LeftArrow.Visible = true;
                        RightArrow.Visible = true;
                        NameTextCurrentState = TextMedium.VariableState.Selected;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// If the current index is valid, returns
        /// the player in the GlobalData.SavedPlayers list
        /// </summary>
        public PlayerData SelectedPlayer
        {
            get
            {
                if(SelectedIndex >= 0 && SelectedIndex < GlobalData.SavedPlayers.Count)
                {
                    return GlobalData.SavedPlayers[SelectedIndex];
                }
                return null;
            }
        }

        /// <summary>
        /// If the SelectedPlayer is valid, returns the name
        /// of the selected player. Otherwise it returns the
        /// CreatePlayerString
        /// </summary>
        public string SelectedPlayerName
        {
            get
            {
                if(SelectedPlayer != null)
                {
                    return SelectedPlayer.Name;
                }

                return NoPlayerString;
            }
        }

        /// <summary>
        /// Listen for left and right press to switch player
        /// </summary>
        protected override void DoGamepadInput(Xbox360GamePad gamepad)
        {
            if(gamepad.LeftStick.AsDPadDown(Xbox360GamePad.DPadDirection.Left))
            {
                LeftArrow.CurrentState = ArrowButton.VariableState.Pressed;
            }
            else
            {
                LeftArrow.CurrentState = ArrowButton.VariableState.Normal;
            }

            if(gamepad.LeftStick.AsDPadDown(Xbox360GamePad.DPadDirection.Right))
            {
                RightArrow.CurrentState = ArrowButton.VariableState.Pressed;
            }
            else
            {
                RightArrow.CurrentState = ArrowButton.VariableState.Normal;
            }

            if(gamepad.LeftStick.AsDPadPushed(Xbox360GamePad.DPadDirection.Left))
            {
                SelectedIndex--;
            }
            if(gamepad.LeftStick.AsDPadPushed(Xbox360GamePad.DPadDirection.Right))
            {
                SelectedIndex++;
            }
        }

        /// <summary>
        /// Updates the displayed name.
        /// Called when the underlying data or the selected index has changed
        /// </summary>
        public void UpdateSelection()
        {
            NameText.DisplayText = SelectedPlayerName;
        }

    }
}
