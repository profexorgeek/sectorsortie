using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
namespace SectorSortie.Entities.Gui
{
    public partial class CharacterSelector
    {
        void OnAfterCharacterIndexSet(object sender, EventArgs e) {
            if(CharacterIndex >= AllowedCharacters.Length) {
                mCharacterIndex = 0;
            }
            if(CharacterIndex < 0) {
                mCharacterIndex = AllowedCharacters.Length - 1;
            }

            CharacterText.DisplayText = AllowedCharacters[CharacterIndex].ToString();
        }
    }
}
