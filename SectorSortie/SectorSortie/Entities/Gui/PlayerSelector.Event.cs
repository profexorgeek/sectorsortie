using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
using SectorSortie.DataTypes;
namespace SectorSortie.Entities.Gui
{
	public partial class PlayerSelector
	{
        /// <summary>
        /// Updates the NameText with the selected players name or shows
        /// the create new player option
        /// </summary>
		void OnAfterPlayerIndexSet (object sender, EventArgs e)
        {
            if(SelectedIndex > GlobalData.SavedPlayers.Count - 1)
            {
                mSelectedIndex = -1;
            }

            if(SelectedIndex < -1)
            {
                mSelectedIndex = GlobalData.SavedPlayers.Count - 1;
            }

            UpdateSelection();
        }
	}
}
