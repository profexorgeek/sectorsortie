using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;


#endif

namespace SectorSortie.Entities.Gui
{
	public partial class NameSelector
	{
		private void CustomInitialize()
		{
            ClearCharacters();
		}

		private void CustomActivity() {}

        private void CustomDestroy() { }

        private static void CustomLoadStaticContent(string contentManagerName) { }

        public override MenuItemBase.VisualState CurrentVisualState
        {
            get
            {
                return mCurrentVisualState;
            }
            set
            {
                mCurrentVisualState = value;
                switch(mCurrentVisualState)
                {
                    case VisualState.Normal:
                    case VisualState.Pressed:
                        TitleText.CurrentState = TextMedium.VariableState.Normal;
                        ClearSelectedState();
                        break;
                    case VisualState.Selected:
                        TitleText.CurrentState = TextMedium.VariableState.Selected;
                        UpdateSelectedState();
                        break;
                    default :
                        break;
                }
            }
        }

        /// <summary>
        /// Returns the string created by
        /// individual character selection
        /// </summary>
        public string SelectedName
        {
            get
            {
                string chars = "";
                for(int i = 0; i < Characters.Count; i++)
                {
                    chars += Characters[i].DisplayText;
                }
                return chars;
            }
        }

        /// <summary>
        /// Listens to the right stick gamepad input to change characters
        /// </summary>
        /// <param name="gamepad">An Xbox360 gamepad</param>
        protected override void DoGamepadInput(Xbox360GamePad gamepad)
        {
            // move between characters
            if(gamepad.RightStick.AsDPadPushedRepeatRate(Xbox360GamePad.DPadDirection.Left))
            {
                SelectedIndex--;
            }
            if(gamepad.RightStick.AsDPadPushedRepeatRate(Xbox360GamePad.DPadDirection.Right))
            {
                SelectedIndex++;
            }

            // change character
            if(gamepad.RightStick.AsDPadPushedRepeatRate(Xbox360GamePad.DPadDirection.Down))
            {
                Characters[SelectedIndex].CharacterIndex--;
            }
            if(gamepad.RightStick.AsDPadPushedRepeatRate(Xbox360GamePad.DPadDirection.Up))
            {
                Characters[SelectedIndex].CharacterIndex++;
            }
        }

        /// <summary>
        /// Sets all characters to "empty" state
        /// </summary>
        public void ClearCharacters() {
            foreach(CharacterSelector selector in Characters) {
                selector.CharacterIndex = 0;
            }
        }

        /// <summary>
        /// Sets all of the character selectors to normal state
        /// Called when changing the selected index.
        /// </summary>
        private void ClearSelectedState() {
            foreach(CharacterSelector selector in Characters) {
                selector.CurrentState = CharacterSelector.VariableState.Normal;
            }
        }

        /// <summary>
        /// Clears then sets selected Character control
        /// </summary>
        private void UpdateSelectedState()
        {
            ClearSelectedState();
            Characters[SelectedIndex].CurrentState = CharacterSelector.VariableState.Selected;
        }

	}
}
