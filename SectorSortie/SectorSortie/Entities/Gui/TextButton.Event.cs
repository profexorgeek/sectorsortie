using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
namespace SectorSortie.Entities.Gui
{
    public partial class TextButton
    {
        /// <summary>
        /// Automatically resize the button
        /// </summary>
        void OnAfterWidthSet (object sender, EventArgs e)
        {
            float endWidth = LeftEnd.Texture.Width;
            
            Middle.Width = Width - endWidth * 2;
            LeftEnd.RelativeX = -Width / 2f + endWidth;
            RightEnd.RelativeX = Width / 2f - endWidth;
            Indicator.RelativeX = LeftEnd.RelativeX;
            ButtonText.MaxWidth = Width;
        }
    }
}
