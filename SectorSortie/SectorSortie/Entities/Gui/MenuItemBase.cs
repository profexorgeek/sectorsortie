using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;


#endif

namespace SectorSortie.Entities.Gui
{
	public partial class MenuItemBase
	{

        public enum VisualState
        {
            Normal,
            Selected,
            Pressed,
            Disabled
        }
        protected VisualState mCurrentVisualState;
        protected bool mIsDisabled;

		private void CustomInitialize()
		{
            if(IsDisabled)
            {
                CurrentVisualState = VisualState.Disabled;
            }
            else
            {
                CurrentVisualState = VisualState.Normal;
            }

		}

		private void CustomActivity()
		{
            if(!IsDisabled)
            {
                DoInputHandling();
            }
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {

        }

        /// <summary>
        /// Overridden by children to control the visual state of the MenuItem
        /// </summary>
        public virtual VisualState CurrentVisualState
        {
            get
            {
                return mCurrentVisualState;
            }
            set
            {
                mCurrentVisualState = value;
            }
        }

        /// <summary>
        /// Controls whether or not this MenuItem is selectable and interactive
        /// </summary>
        public virtual bool IsDisabled
        {
            get
            {
                return mIsDisabled;
            }
            set
            {
                mIsDisabled = value;
                if(mIsDisabled)
                {
                    CurrentVisualState = VisualState.Disabled;
                }
            }
        }

        /// <summary>
        /// Enables ClickNoSlide to be called in code
        /// </summary>
        public void CallClickNoSlide()
        {
            if(this.ClickNoSlide != null)
            {
                ClickNoSlide(this);
            }
        }

        /// <summary>
        /// Can be overridden to change the type of input handling the menu
        /// item implements.
        /// </summary>
        protected virtual void DoInputHandling()
        {
            foreach(Xbox360GamePad gamepad in InputManager.Xbox360GamePads)
            {
                if(gamepad.IsConnected && !IsDisabled && CurrentVisualState == VisualState.Selected)
                {
                    DoGamepadInput(gamepad);
                }
            }
        }

        /// <summary>
        /// Can be overridden to handle input from the provided gamepad.
        /// By default the MenuItemBase calls this for any connected gamepad
        /// if the menu item is selected and not disabled
        /// </summary>
        /// <param name="gamepad">An Xbox360 gamepad</param>
        protected virtual void DoGamepadInput(Xbox360GamePad gamepad)
        {

        }
	}
}
