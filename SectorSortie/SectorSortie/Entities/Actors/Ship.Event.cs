using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Screens;
using SectorSortie.Extensions;
namespace SectorSortie.Entities.Actors
{
	public partial class Ship
	{
		void OnAfterShipTypeSet (object sender, EventArgs e)
        {            
            // set game variables
            mCurrentEnergy = ShipType.MaxEnergy;
            mCurrentShields = ShipType.MaxShield;
            ShieldCollision.Radius = ShipType.ShieldRadius;

            // load and scale sprites
            ShipSprite.SwapTexture(string.Format(@"/content/ships/{0}_body.png", ShipType.Name.ToLower()));
            EngineSprite.SwapTexture(string.Format(@"/content/ships/{0}_engine.png", ShipType.Name.ToLower())); 
            ShieldSprite.SwapTexture(string.Format(@"/content/ships/{0}_shield.png", ShipType.Name.ToLower())); 
        }
        
        void OnAfterCurrentStateSet (object sender, EventArgs e)
        {
            switch(CurrentState) {
                case VariableState.Active :
                    Visible = true;
                    break;
                case VariableState.Dead :
                    Visible = false;
                    break;
                case VariableState.Disabled :
                    Visible = false;
                    break;
            }
        }

	}
}
