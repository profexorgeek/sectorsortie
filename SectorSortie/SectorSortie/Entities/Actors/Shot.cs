using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using SectorSortie.DataTypes;


#endif

namespace SectorSortie.Entities.Actors
{
	public partial class Shot
	{
        
        private Ship mOwner;

		private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{


		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public Ship Owner
        {
            get
            {
                return mOwner;
            }
            set
            {
                mOwner = value;
            }
        }

        public void SetupShot(Vector3 position, float rotation, ShotTypes type, Ship owner) {
            ShotType = type;
            Position = position;
            RotationZ = rotation;
            Velocity.X = (float)(Math.Cos(RotationZ) * ShotType.Speed);
            Velocity.Y = (float)(Math.Sin(RotationZ) * ShotType.Speed);
            ShotCollision.Radius = ShotType.CollisionRadius;
            Owner = owner;
        }
	}
}
