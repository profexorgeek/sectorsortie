using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using SectorSortie.Factories;
using SectorSortie.DataTypes;


#endif

namespace SectorSortie.Entities.Actors
{
	public partial class Ship
	{
        private float mCurrentShields;
        private float mCurrentEnergy;

        private double mTimeToFrontShot;
        private double mTimeToRightShot;
        private double mTimeToLeftShot;
        private double mTimeToRearShot;

        private PlayerData mPlayerData;

        const float forwardRotation = (float)(Math.PI / 2);
        const float rightRotation = (float)(Math.PI / 4);
        const float leftRotation = rightRotation * 3f;
        const float rearRotation = -forwardRotation;

        


		private void CustomInitialize()
		{
            this.Drag = Screens.SortieScreen.LevelDrag;
            FireTrailInstance.IsEmitting = false;
		}

		private void CustomActivity()
		{
            if(CurrentState == VariableState.Active) {
                DoEffects();
                DoTimedActivities();
            }
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public PlayerData PlayerDataReference
        {
            get
            {
                return mPlayerData;
            }
            set
            {
                mPlayerData = value;
            }
        }

        public int TotalLevel
        {
            get
            {
                int level = ShipType.ShipLevel;

                if(FrontWeapon != null)
                {
                    level += FrontWeapon.ShotLevel;
                }

                if(RearWeapon != null)
                {
                    level += RearWeapon.ShotLevel;
                }

                if(RightWeapon != null)
                {
                    level += RightWeapon.ShotLevel;
                }

                if(LeftWeapon != null)
                {
                    level += LeftWeapon.ShotLevel;
                }
                return level;
            }
        }

        public void Damage(float amount) {
            ShieldSprite.Alpha = 1;
            mCurrentShields -= amount;

            if(mCurrentShields <= 0) {
                Die();
            }
        }

        public void DoEffects() {
            if(Acceleration.Y > 0) {
                EngineSprite.Visible = true;
            }
            else {
                EngineSprite.Visible = false;
            }

            if(mCurrentShields < PercentToShowDamage * ShipType.MaxShield)
            {
                FireTrailInstance.IsEmitting = true;
            }
            else
            {
                FireTrailInstance.IsEmitting = false;
            }
        }

        public void FireAllWeapons() {
            FireFrontWeapon();
            FireLeftWeapon();
            FireRightWeapon();
            FireRearWeapon();
        }

        public void FireFrontWeapon() {
            if(FrontWeapon != null && mTimeToFrontShot <= 0 && mCurrentEnergy > FrontWeapon.Energy) {
                mCurrentEnergy -= FrontWeapon.Energy;
                Shot s = ShotFactory.CreateNew();
                s.SetupShot(Position, forwardRotation, FrontWeapon, this);
                mTimeToFrontShot = FrontWeapon.ReloadTime;
            }
        }

        public void FireLeftWeapon() {
            if(LeftWeapon != null && mTimeToLeftShot <= 0 && mCurrentEnergy > LeftWeapon.Energy) {
                mCurrentEnergy -= LeftWeapon.Energy;
                Shot s = ShotFactory.CreateNew();
                s.SetupShot(Position, leftRotation, LeftWeapon, this);
                mTimeToLeftShot = LeftWeapon.ReloadTime;
            }
        }

        public void FireRightWeapon() {
            if(RightWeapon != null && mTimeToRightShot <= 0 && mCurrentEnergy > RightWeapon.Energy) {
                mCurrentEnergy -= RightWeapon.Energy;
                Shot s = ShotFactory.CreateNew();
                s.SetupShot(Position, rightRotation, RightWeapon, this);
                mTimeToRightShot = RightWeapon.ReloadTime;
            }
        }

        public void FireRearWeapon() {
            if(RearWeapon != null && mTimeToRearShot <= 0 && mCurrentEnergy > RearWeapon.Energy) {
                mCurrentEnergy -= RearWeapon.Energy;
                Shot s = ShotFactory.CreateNew();
                s.SetupShot(Position, rearRotation, RearWeapon, this);
                mTimeToRearShot = RearWeapon.ReloadTime;
            }
        }

        public void DoTimedActivities() {
            // fade shield
            if(ShieldSprite.Alpha > 0) {
                ShieldSprite.Alpha -= ShieldFadeSpeed * TimeManager.SecondDifference;
            }

            // regenerate energy and sheilds
            mCurrentEnergy += ShipType.EnergyPerSecond * TimeManager.SecondDifference;
            mCurrentShields += ShipType.ShieldPerSecond * TimeManager.SecondDifference;

            // reload weapons
            mTimeToFrontShot -= TimeManager.SecondDifference;
            mTimeToLeftShot -= TimeManager.SecondDifference;
            mTimeToRightShot -= TimeManager.SecondDifference;
            mTimeToRearShot -= TimeManager.SecondDifference;

            // clamp values
            mCurrentShields = Math.Min(ShipType.MaxShield, mCurrentShields);
            mCurrentEnergy = Math.Min(ShipType.MaxEnergy, mCurrentEnergy);
            ShieldSprite.Alpha = Math.Max(0, ShieldSprite.Alpha);
            mTimeToFrontShot = Math.Max(0, mTimeToFrontShot);
            mTimeToLeftShot = Math.Max(0, mTimeToLeftShot);
            mTimeToRightShot = Math.Max(0, mTimeToRightShot);
            mTimeToRearShot = Math.Max(0, mTimeToRearShot);
        }

        public void Die() {
            Console.WriteLine("Ship has died.");
            CurrentState = VariableState.Dead;
            ShipExplosionInstance.Emit();
            Destroy();
        }
	}
}
