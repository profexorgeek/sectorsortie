using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
using SectorSortie.Extensions;
namespace SectorSortie.Entities.Actors
{
	public partial class Shot
	{
		void OnAfterShotTypeSet (object sender, EventArgs e)
        {
            ShotSprite.SwapTexture(string.Format("content/shots/{0}.png", ShotType.ShotTexture));
        }

	}
}
