using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;


#endif

namespace SectorSortie.Entities.Scenery
{
	public partial class Star
	{


		private void CustomInitialize()
		{
            Position.Z = FlatRedBallServices.Random.Next(MinZ, MaxZ);
            Position.Y = RandomYAtZ();
            Position.X = RandomXAtZ();
            RotationZ = (float)FlatRedBallServices.Random.NextDouble();
            StarSprite.Alpha = (float)FlatRedBallServices.Random.Next(MinStarBrightness, MaxStarBrightness) / 100f;
		}

		private void CustomActivity()
		{
            CheckWrapStar();
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private float RandomXAtZ() {
            return FlatRedBallServices.Random.Next((int)Camera.Main.AbsoluteLeftXEdgeAt(Position.Z), (int)Camera.Main.AbsoluteRightXEdgeAt(Position.Z));
        }

        private float RandomYAtZ() {
            return FlatRedBallServices.Random.Next((int)Camera.Main.AbsoluteBottomYEdgeAt(Position.Z), (int)Camera.Main.AbsoluteTopYEdgeAt(Position.Z));
        }

        private float ScreenHeightAtPosition() {
            return Camera.Main.AbsoluteTopYEdgeAt(Position.Z) - Camera.Main.AbsoluteBottomYEdgeAt(Position.Z);
        }

        private float ScreenWidthAtPosition() {
            return Camera.Main.AbsoluteRightXEdgeAt(Position.Z) - Camera.Main.AbsoluteLeftXEdgeAt(Position.Z);
        }

        private void CheckWrapStar() {
            if(Position.Y < Camera.Main.AbsoluteBottomYEdgeAt(Position.Z) - this.StarSprite.Texture.Height) {
                this.Position.Y = Camera.Main.AbsoluteTopYEdgeAt(Position.Z) + this.StarSprite.Texture.Height;
                this.Position.X = RandomXAtZ();
            }
        }
	}
}
