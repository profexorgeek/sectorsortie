﻿using FlatRedBall;
using FlatRedBall.Content;
using FlatRedBall.Math;
using SectorSortie.DataTypes;
using SectorSortie.Entities.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SectorSortie.Extensions;

namespace SectorSortie.Spawning
{
    public class Spawner
    {
        private PositionedObjectList<Ship> mEnemies;
        private PositionedObjectList<Ship> mPlayers;
        private float mTimeElapsed;
        private string mContentManagerName;

        public Spawner(PositionedObjectList<Ship> playerReference, PositionedObjectList<Ship> enemyReference, string contentManagerName)
        {
            mPlayers = playerReference;
            mEnemies = enemyReference;
            mContentManagerName = contentManagerName;
            mTimeElapsed = 0;
        }

        public int PlayerLevel
        {
            get
            {
                int level = 0;
                foreach(Ship player in mPlayers)
                {
                    // count both active and dead players because difficulty shouldn't go down if a player dies
                    if(player.CurrentState == Ship.VariableState.Active || player.CurrentState == Ship.VariableState.Dead)
                    {
                        level += player.TotalLevel;
                    }
                }
                return level;
            }
        }

        public int TargetLevel
        {
            get
            {
                return PlayerLevel * (int)(mTimeElapsed / Screens.SortieScreen.SecondsToDifficultyIncrease + 1);
            }
        }

        public void Activity()
        {
            mTimeElapsed += TimeManager.SecondDifference;
            if(mEnemies.Count == 0)
            {
                SpawnWave();
            }
        }

        public void SpawnWave()
        {
            FlatRedBall.Debugging.Debugger.Write("Spawning wave with value: " + TargetLevel);

            int waveLevel = 0;
            // create ships until we hit the target level
            // each ship can have any strength up to the points left
            while(waveLevel <= TargetLevel)
            {
                // calculate how many points we have left to spend
                int pointsThisShip = TargetLevel - waveLevel;

                // select the maximum level of ship we can choose, leaving points for weapons
                int maxShipLevel = Math.Max(1, (int)(pointsThisShip * 0.75f));

                // get all ship options less than specified level
                List<ShipTypes> validShips = Enumerable.ToList(GlobalContent.ShipTypes.Values).Where(s => s.ShipLevel <= maxShipLevel).ToList();
                ShipTypes shipType = GetRandomListItem(validShips);
                Ship ship = new Ship(mContentManagerName);
                ship.ShipType = shipType;

                // select the maximum level of weapon we can choose
                int pointsThisShot = TargetLevel - waveLevel - shipType.ShipLevel;
                int maxShotLevel = Math.Max(1, pointsThisShot);
                List<ShotTypes> validShots = Enumerable.ToList(GlobalContent.ShotTypes.Values).Where(s => s.ShotLevel <= maxShotLevel).ToList();
                ShotTypes shotType = GetRandomListItem(validShots);

                // TODO: add chance to have multiple weapons
                ship.FrontWeapon = shotType;
                ship.RotationZ = (float)-Math.PI;
                ship.YAcceleration = -ship.ShipType.MaxThrust / 4;
                // TODO: Don't hardcode spawn area
                ship.Position = Camera.Main.RandomPositionAboveCameraAtZ(Screens.SortieScreen.ScreenPadding);
                mEnemies.Add(ship);
                waveLevel += ship.TotalLevel;
            }

            // TODO: add ships to formations
        }

        private T GetRandomListItem<T>(List<T> items)
        {
            int index = FlatRedBallServices.Random.Next(0, items.Count);
            return items[index];
        }

        
    }
}
