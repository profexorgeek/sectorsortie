﻿using FlatRedBall;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.Extensions
{
    public static class SpriteExtensions
    {
        public static void SwapTexture(this Sprite sprite, string texturePath, bool autoScale = true) {
            sprite.Texture = FlatRedBallServices.Load<Texture2D>(texturePath);
            if(autoScale) {
                sprite.Width = sprite.Texture.Width;
                sprite.Height = sprite.Texture.Height;
            }
        }
    }
}
