﻿using FlatRedBall;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.Extensions
{
    public static class CameraExtensions
    {
        /// <summary>
        /// Finds a random position that will be within the visual area on the X axis but above the visible area on the Y axis
        /// </summary>
        /// <param name="camera">The camera to use in calculations</param>
        /// <param name="padding">The amount of internal padding on the X axis and external padding on the Y axis</param>
        /// <param name="z">The z index to calculate at</param>
        /// <returns>A new Vector3 position</returns>
        public static Vector3 RandomPositionAboveCameraAtZ(this Camera camera, float padding = 0, float z = 0)
        {
            float x = RandomOnscreenXAtZ(camera, padding, z);
            float y = camera.AbsoluteTopYEdgeAt(0) + padding;
            return new Vector3(x, y, z);
        }

        public static Vector3 RandomPositionOnscreenAtZ(this Camera camera, float padding = 0, float z = 0)
        {
            float x = RandomOnscreenXAtZ(camera, padding, z);
            float y = RandomOnscreenYAtZ(camera, padding, z);
            return new Vector3(x, y, z);
        }

        public static float RandomOnscreenXAtZ(this Camera camera, float padding = 0, float z = 0)
        {
            float screenWidth = camera.AbsoluteRightXEdgeAt(z) - camera.AbsoluteLeftXEdgeAt(z) + padding;
            float x = camera.AbsoluteLeftXEdgeAt(0) + (float)(FlatRedBallServices.Random.NextDouble() * screenWidth) + padding / 2;
            return x;
        }

        public static float RandomOnscreenYAtZ(this Camera camera, float padding = 0, float z = 0)
        {
            float screenHeight = camera.AbsoluteTopYEdgeAt(z) - camera.AbsoluteBottomYEdgeAt(z) + padding;
            float y = (float)(FlatRedBallServices.Random.NextDouble() * screenHeight) + padding / 2;
            return y;
        }


    }
}
