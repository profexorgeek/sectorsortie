﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.Logging
{
    public class BasicLogger : ILogger
    {

        public void LogInfo(string msg) {
            Console.WriteLine("INFO: " + msg);
        }

        public void LogWarning(string msg) {
            Console.WriteLine("WARNING: " + msg);
        }

        public void LogError(string msg) {
            Console.WriteLine("ERROR: " + msg);
        }
    }
}
