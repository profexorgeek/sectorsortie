﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SectorSortie.Logging
{
    public interface ILogger
    {
        void LogInfo(string msg);
        void LogWarning(string msg);
        void LogError(string msg);
    }
}
