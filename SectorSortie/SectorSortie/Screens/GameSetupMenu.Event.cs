using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
using SectorSortie.DataTypes;
namespace SectorSortie.Screens
{
	public partial class GameSetupMenu
	{
		void OnBackButtonClickNoSlide (FlatRedBall.Gui.IWindow callingWindow)
        {
            MoveToScreen(typeof(Screens.MainMenu).FullName);
        }

        void OnPlayButtonClickNoSlide (FlatRedBall.Gui.IWindow callingWindow)
        {
            // TODO: check no overlapping players
            foreach(MenuItemBase menuItem in MenuItems)
            {
                if(menuItem is PlayerSelector)
                {
                    PlayerSelector selector = menuItem as PlayerSelector;
                    if(selector.SelectedPlayer != null)
                    {
                        selector.SelectedPlayer.InUse = true;
                        selector.SelectedPlayer.PlayerIndex = selector.PlayerIndex;
                    }
                }
            }

            MoveToScreen(typeof(Screens.SortieScreen).FullName);
        }

	}
}
