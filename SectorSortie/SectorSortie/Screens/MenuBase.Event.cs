using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
namespace SectorSortie.Screens
{
	public partial class MenuBase
	{
        /// <summary>
        /// Sets the selected state on the target component, if valid
        /// </summary>
		void OnAfterSelectedMenuItemSet (object sender, EventArgs e)
        {
            // early out
            if(MenuItems == null)
            {
                return;
            }

            // wrap negative selection
            if(SelectedMenuItem < 0)
            {
                mSelectedMenuItem = MenuItems.Count - 1;
            }

            // advance the selection if the newly-selected component is disabled
            if(MenuItems.Count > SelectedMenuItem && MenuItems[SelectedMenuItem].IsDisabled)
            {
                mSelectedMenuItem++;
            }

            // wrap selection
            if(SelectedMenuItem > MenuItems.Count - 1)
            {
                mSelectedMenuItem = 0;
            }

            // set new selected status
            UpdateSelectedItem();
        }

        /// <summary>
        /// Updates the displayed menu name and orients it to the side of the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnAfterMenuNameSet (object sender, EventArgs e)
        {
            MenuTitle.DisplayText = MenuName;
        }

	}
}
