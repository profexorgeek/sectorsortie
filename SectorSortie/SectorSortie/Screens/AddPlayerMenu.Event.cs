using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
using SectorSortie.DataTypes;
namespace SectorSortie.Screens
{
	public partial class AddPlayerMenu
	{
        void OnBackButtonClickNoSlide (FlatRedBall.Gui.IWindow callingWindow)
        {
            MoveToScreen(typeof(Screens.ManagePlayersMenu).FullName);
        }

        void OnNewPlayerButtonClickNoSlide (FlatRedBall.Gui.IWindow callingWindow)
        {
            try
            {
                GlobalData.AddNewPlayer(NewPlayerName.SelectedName);
                NewPlayerName.ClearCharacters();
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to create player: " + e.Message);
            }
        }

	}
}
