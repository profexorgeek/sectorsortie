using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Screens;
namespace SectorSortie.Screens
{
	public partial class BaseScreen
	{
		void OnResolutionOrOrientationChanged (object sender, EventArgs e)
        {
            UpdateScreenDimensions();
        }

	}
}
