using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Graphics.Model;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using SectorSortie.Entities.Gui;
#endif

namespace SectorSortie.Screens
{
	public partial class MenuBase
	{

		void CustomInitialize()
		{
            UpdateSelectedItem();

		}

		void CustomActivity(bool firstTimeCalled)
		{
            DoMenuInput();
		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        /// <summary>
        /// Clears the selected status of any item that is currently selected.
        /// Assumes that if an item can be selected, it's default state is Normal
        /// </summary>
        protected void ClearSelectedStatus()
        {
            foreach(MenuItemBase item in MenuItems)
            {
                if(!item.IsDisabled)
                {
                    item.CurrentVisualState = MenuItemBase.VisualState.Normal;
                }
            }
        }

        /// <summary>
        /// Sets the visual state of the currently selected item
        /// </summary>
        protected void UpdateSelectedItem()
        {
            ClearSelectedStatus();
            if(MenuItems != null && SelectedMenuItem < MenuItems.Count && SelectedMenuItem >= 0)
            {
                if(MenuItems[SelectedMenuItem].CurrentVisualState != MenuItemBase.VisualState.Disabled)
                {
                    MenuItems[SelectedMenuItem].CurrentVisualState = MenuItemBase.VisualState.Selected;
                }
            }
        }

        /// <summary>
        /// Listens for menu input from any connected gamepad
        /// </summary>
        protected void DoMenuInput()
        {

            foreach(Xbox360GamePad gamepad in InputManager.Xbox360GamePads)
            {
                if(gamepad.IsConnected)
                {
                    if(gamepad.LeftStick.AsDPadPushed(Xbox360GamePad.DPadDirection.Up))
                    {
                        SelectedMenuItem--;
                    }

                    if(gamepad.LeftStick.AsDPadPushed(Xbox360GamePad.DPadDirection.Down))
                    {
                        SelectedMenuItem++;
                    }

                    if(gamepad.ButtonDown(Xbox360GamePad.Button.A) && !MenuItems[SelectedMenuItem].IsDisabled)
                    {
                        MenuItems[SelectedMenuItem].CurrentVisualState = MenuItemBase.VisualState.Pressed;
                    }
                    else
                    {
                        MenuItems[SelectedMenuItem].CurrentVisualState = MenuItemBase.VisualState.Selected;
                    }

                    if(gamepad.ButtonPushed(Xbox360GamePad.Button.A))
                    {
                        MenuItems[SelectedMenuItem].CallClickNoSlide();
                    }
                }
            }
        }
	}
}
