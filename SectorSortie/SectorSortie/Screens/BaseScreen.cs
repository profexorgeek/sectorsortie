using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Graphics.Model;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using Microsoft.Xna.Framework;
using SectorSortie.Entities.Scenery;
#endif

namespace SectorSortie.Screens
{
	public partial class BaseScreen
	{

		void CustomInitialize()
		{
            UpdateScreenDimensions();
            GenerateStars();
		}

		void CustomActivity(bool firstTimeCalled)
		{


		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void UpdateScreenDimensions() {
            Camera.Main.UsePixelCoordinates3D(0);
            Camera.Main.FarClipPlane = 5000;
            VignetteSprite.Width = Camera.Main.AbsoluteRightXEdgeAt(VignetteSprite.Z) - Camera.Main.AbsoluteLeftXEdgeAt(VignetteSprite.Z);
            VignetteSprite.Height = Camera.Main.AbsoluteTopYEdgeAt(VignetteSprite.Z) - Camera.Main.AbsoluteBottomYEdgeAt(VignetteSprite.Z);
            VignetteSprite.Alpha = 0.5f;
            Camera.Main.BackgroundColor = new Color(0.184f, 0.224f, 0.255f);
        }

        public void GenerateStars() {
            for(int i = 0; i < NumberOfStars; i++) {
                Star s = new Star(ContentManagerName);
                Stars.Add(s);
            }

            SetStarScrollSpeed(100);
        }

        public void SetStarScrollSpeed(float speed) {
            foreach(Star s in Stars) {
                s.Velocity.Y = -speed;
            }
        }
	}
}
