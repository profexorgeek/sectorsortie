using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Graphics.Model;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using SectorSortie.Entities.Actors;
using SectorSortie.DataTypes;
using SectorSortie.Factories;
using SectorSortie.Spawning;
#endif

namespace SectorSortie.Screens
{
	public partial class SortieScreen
	{
        private Spawner mSpawner;
        private List<PlayerData> mPlayersInGame;

		void CustomInitialize()
		{

            SetupGame();
		}

		void CustomActivity(bool firstTimeCalled)
		{
            if(firstTimeCalled) {

            }
            DoPlayerInput();
            mSpawner.Activity();
            RemoveObjectsOffscreen();
            CollideShots();
            CollideShips();
            LimitPlayerPosition();
		}

		void CustomDestroy()
		{
            
		}

        static void CustomLoadStaticContent(string contentManagerName)
        {

        }


        private void SetupGame()
        {
            ShotFactory.Initialize(Shots, ContentManagerName);
            mSpawner = new Spawner(Players, Enemies, ContentManagerName);
            mPlayersInGame = GlobalData.SavedPlayers.Where(p => p.InUse == true).ToList();
            foreach(PlayerData playerData in mPlayersInGame)
            {
                Ship playerShip = new Ship(ContentManagerName);
                playerShip.ShipType = playerData.Ship.ShipType;
                playerShip.FrontWeapon = playerData.Ship.FrontWeapon;
                playerShip.LeftWeapon = playerData.Ship.LeftWeapon;
                playerShip.RightWeapon = playerData.Ship.RightWeapon;
                playerShip.RearWeapon = playerData.Ship.RearWeapon;
                playerShip.ControllerIndex = playerData.PlayerIndex;
                playerShip.PlayerDataReference = playerData;
                playerShip.CurrentState = Ship.VariableState.Active;
                Players.Add(playerShip);
            }
        }

        private void CollideShots()
        {
            for(int i = Shots.Count - 1; i > -1; i--)
            {
                Shot currentShot = Shots[i];
                
                // the shot belongs to an enemy
                if(currentShot.Owner.ControllerIndex == -1)
                {
                    for(int j = Players.Count - 1; j > -1; j--)
                    {
                        Ship player = Players[j];

                        if(currentShot.ShotCollision.CollideAgainst(player.ShieldCollision))
                        {
                            player.Damage(currentShot.ShotType.Damage);
                            currentShot.Destroy();
                        }
                    }
                }
                // the shot belongs to a player
                else
                {
                    for(int j = Enemies.Count - 1; j > -1; j--)
                    {
                        Ship enemy = Enemies[j];
                        if(currentShot.ShotCollision.CollideAgainst(enemy.ShieldCollision))
                        {
                            enemy.Damage(currentShot.ShotType.Damage);
                            currentShot.Destroy();
                        }
                    }
                }
            }
        }

        private void CollideShips()
        {
            for(int i = Enemies.Count - 1; i > -1; i--)
            {
                Ship enemy = Enemies[i];
                for(int j = Players.Count - 1; j > -1; j--)
                {
                    Ship player = Players[j];
                    player.ShieldCollision.CollideAgainstBounce(enemy.ShieldCollision, 0.5f, 0.5f, 0.5f);
                }
            }
        }

        private void RemoveObjectsOffscreen()
        {
            for(int i = Enemies.Count - 1; i > -1; i--)
            {
                // TODO: don't hardcode padding value
                if(Enemies[i].Y < Camera.Main.AbsoluteBottomYEdgeAt(Enemies[i].Position.Z) - ScreenPadding)
                {
                    Enemies[i].Destroy();
                }
            }

            for(int i = Shots.Count - 1; i > -1; i--) {
                if(Shots[i].Y > Camera.Main.AbsoluteTopYEdgeAt(Shots[i].Position.Z) + ScreenPadding ||
                    Shots[i].Y < Camera.Main.AbsoluteBottomYEdgeAt(Shots[i].Position.Z) - ScreenPadding)
                {
                    Shots[i].Destroy();
                }
            }

        }

        private void DoPlayerInput()
        {
            foreach(Ship playerShip in Players)
            {
                playerShip.Acceleration.X = InputManager.Xbox360GamePads[playerShip.ControllerIndex].LeftStick.Position.X * playerShip.ShipType.MaxThrust;
                playerShip.Acceleration.Y = InputManager.Xbox360GamePads[playerShip.ControllerIndex].LeftStick.Position.Y * playerShip.ShipType.MaxThrust;

                if(InputManager.Xbox360GamePads[playerShip.ControllerIndex].RightTrigger.Position > 0)
                {
                    playerShip.FireAllWeapons();
                }
            }
        }

        private void LimitPlayerPosition()
        {
            foreach(Ship player in Players)
            {
                if(player.CurrentState == Ship.VariableState.Active)
                {
                    float topLimit = Camera.Main.AbsoluteTopYEdgeAt(player.Z) - ScreenPadding;
                    float bottomLimit = Camera.Main.AbsoluteBottomYEdgeAt(player.Z) + ScreenPadding;
                    float screenWidth = Camera.Main.AbsoluteRightXEdgeAt(player.Z) - Camera.Main.AbsoluteLeftXEdgeAt(player.Z);

                    player.Y = Math.Max(player.Y, bottomLimit);
                    player.Y = Math.Min(player.Y, topLimit);

                    if(player.X > Camera.Main.AbsoluteRightXEdgeAt(player.Z))
                    {
                        player.X -= screenWidth;
                    }

                    if(player.X < Camera.Main.AbsoluteLeftXEdgeAt(player.Z))
                    {
                        player.X += screenWidth;
                    }
                }
            }
        }
	}
}
