using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using SectorSortie.Entities.Actors;
using SectorSortie.Entities.Gui;
using SectorSortie.Entities.Scenery;
using SectorSortie.Screens;
namespace SectorSortie.Screens
{
	public partial class SettingsMenu
	{
        void OnBackButtonClickNoSlide (FlatRedBall.Gui.IWindow callingWindow)
        {
            MoveToScreen(typeof(Screens.MainMenu).FullName);
        }

	}
}
